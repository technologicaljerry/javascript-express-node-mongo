// routes/auth.js
const express = require('express');
const passport = require('passport');
const bcrypt = require('bcryptjs');
const router = express.Router();
const User = require('../config/user.schema');

router.post('/login', passport.authenticate('local'), (req, res) => {
    res.json(req.user);
});

router.post('/register', (req, res) => {
    const { username, password } = req.body;
    bcrypt.hash(password, 10, (err, hashedPassword) => {
        if (err) throw err;
        const newUser = new User({
            username: username,
            password: hashedPassword
        });
        newUser.save((err, user) => {
            if (err) throw err;
            res.json(user);
        });
    });
});

router.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/');
});

module.exports = router;
